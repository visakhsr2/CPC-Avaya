//
//  Extension.swift
//  Takaful
//
//  Created by Visakh on 28/10/20.
//  Copyright © 2020 BTeem. All rights reserved.
//

import Foundation
import UIKit


extension Notification.Name {
    static let didConnectedToNetworkNotification = Notification.Name("didConnectedToNetworkNotification")
    static let didReceiveNotification = Notification.Name("didReceiveRemoteNotification")
    static let didTapRemoteNotification = Notification.Name("didTapRemoteNotification")
    static let appDidEnterForegroundNotification = Notification.Name("applicationEnteredForeground")
}


extension UIFont {
    
    class func regularFont(size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: size)!
    }
    
    class func semiBoldFont(size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Semibold", size: size)!
    }
    
    class func boldFont(size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Bold", size: size)!
    }
    
    class func mediumFont(size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: size)!
    }
    
    class func font(_ weight: FontWeight, _ size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-\(weight.rawValue)", size: size)!
    }
    
}


enum FontWeight: String {
    case Regular = "Regular"
    case Thin = "Thin"
    case Medium = "Medium"
    case SemiBold = "Semibold"
    case Bold = "Bold"
}

// ******************
// MARK:- UIView
// ******************
extension UIView {
    
    @IBInspectable var cornerRadius: Double {
         get {
           return Double(self.layer.cornerRadius)
         } set {
           self.layer.cornerRadius = CGFloat(newValue)
         }
    }
    
//    @IBInspectable var themeGradient: Bool {
//         get {
//           return false
//         } set {
//            if newValue {
//                self.applyThemeGradient()
//            }
//         }
//    }
//    
//    @IBInspectable var isVerticalGradient: Bool {
//         get {
//           return false
//         } set {
//            if newValue && themeGradient {
//                self.applyThemeGradient(isVerticalDirection: true)
//            }
//         }
//    }
    
    @IBInspectable var borderWidth: Double {
          get {
            return Double(self.layer.borderWidth)
          } set {
           self.layer.borderWidth = CGFloat(newValue)
          }
    }
    
    @IBInspectable var borderColor: UIColor? {
         get {
            return UIColor(cgColor: self.layer.borderColor!)
         } set {
            self.layer.borderColor = newValue?.cgColor
         }
    }
    
    func makeCornersRound() {
        self.layer.cornerRadius = self.frame.height / 2.0
        self.layer.masksToBounds = true
    }
    
    /// Set Shadow
    ///
    /// - Parameters:
    ///   - radius: shadow radius
    ///   - color: shadow color
    ///   - opacity: shadow opacity, shadow won't be visible if opacity is 0
    func setShadowProperties(radius: CGFloat, color: UIColor, opacity: Float, offset : CGSize? = CGSize.zero)  {
        
        onmain {
            
            self.layer.shadowColor   = color.cgColor
            self.layer.shadowOpacity = opacity
            self.layer.shadowOffset  = offset!
            self.layer.shadowRadius  = radius
        }
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func makeRoundedAndShadowed() {
        let shadowLayer = CAShapeLayer()
        self.layer.cornerRadius = self.layer.frame.height / 2
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds,
                                        cornerRadius: self.layer.cornerRadius).cgPath
        shadowLayer.fillColor = self.backgroundColor?.cgColor
        shadowLayer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 5.0
        self.layer.insertSublayer(shadowLayer, at: 0)
        
    }
    
}


// ******************
// MARK:- UIColor
// ******************
extension UIColor {
    
    class var themeTextColor: UIColor {
        return UIColor.init(hexString: "035C8B")!
    }
    
    class var leftGradientColor: UIColor {
        return UIColor(red: 73.0/255.0, green: 83.0/255.0, blue: 156.0/255.0, alpha: 1)
    }
    
    class var rightGradientColor: UIColor {
        return UIColor(red: 27.0/255.0, green: 186.0/255.0, blue: 216.0/255.0, alpha: 1)
    }
    
    class var placeholderGray: UIColor {
        return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
    }
    
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
    
    
}




extension Date {
    
    func toString( dateFormat format  : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    func adding(seconds: Int) -> Date {
        return Calendar.current.date(byAdding: .second, value: seconds, to: self)!
    }
}


extension NSLayoutConstraint {
    
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        NSLayoutConstraint.deactivate([self])
        let newConstraint = NSLayoutConstraint(
            item: firstItem as Any,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)

        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier

        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}


extension Array where Element: Equatable {
    
    @discardableResult mutating func remove(object: Element) -> Bool {
        if let index = firstIndex(of: object) {
            self.remove(at: index)
            return true
        }
        return false
    }
}

extension UIImageView {
    func setRemoteImage(imageURLString:String) {
        //ServiceManager.serviceImageRequest(urlString: imageURLString, imageview: self)
    }
    @IBInspectable var maskImageColor : UIColor {
         get {
           return UIColor()
         } set {
            self.image = self.image?.maskWithColor(color: newValue)
         }
    }
}

extension UITableView {
    func handleEmptyState(dataSourceCount:Int) {
        if dataSourceCount > 0 {
            backgroundView = nil
        } else {
            let label  = UILabel(frame: CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height))
            label.font = UIFont.mediumFont(size: 15)
            label.text          = "Nothing to show here!"
            label.textColor     = UIColor.darkGray
            label.textAlignment = .center
            backgroundView  = label
        }
    }
    func emptyFooter() {
        self.tableFooterView = UIView.init(frame: CGRect.zero)
    }
}

extension UITextField {
    
    
    @IBInspectable var leftPadding: Double {
         get {
           return Double(0)
         } set {
            let paddingView = UIView(frame: CGRect.init(x: 0.0, y: 0.0, width: newValue, height: Double(self.frame.height)))
           self.leftView = paddingView
           self.leftViewMode = .always
         }
    }
    
    @IBInspectable var rightPadding: Double {
         get {
           return Double(0)
         } set {
            let paddingView = UIView(frame: CGRect.init(x: 0.0, y: 0.0, width: newValue, height: Double(self.frame.height)))
           self.rightView = paddingView
           self.rightViewMode = .always
         }
    }
    
    @IBInspectable var placeholderColor: UIColor {
        
        get {
            return UIColor.placeholderGray
        } set {
            
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
            attributes: [NSAttributedString.Key.foregroundColor: newValue])
            
        }
    }
    
    /// To set PlaceHolder color and Text color
    ///
    /// - Parameters:
    ///   - color: Placeholder color
    ///   - text: placeholder text
    ///   - textColor: UITextField text color
    @objc func setPlaceHolderColor( _ color : UIColor, text : String, andtextColor : UIColor) {
        
        self.attributedPlaceholder = NSAttributedString(string: text,
                                                        attributes: [NSAttributedString.Key.foregroundColor: color])
        self.textColor = andtextColor
        
    }
    
    /// To set PlaceHolder color and Text color
    ///
    /// - Parameters:
    ///   - color: Placeholder color
    ///   - text: placeholder text
    ///   - textColor: UITextField text color
    @objc func setPlaceHolderColor( _ color : UIColor, forPlaceHolderText text : String) {
        
        self.attributedPlaceholder = NSAttributedString(string: text,
                                                        attributes: [NSAttributedString.Key.foregroundColor: color])
        
    }
    
    /// If the textField ends with a ".", it has to be properly mapped.
    func setProperDecimalValue(){
        
        // If the first character is a ".", it should be preceeded with a 0
        var inputStr = self.text ?? ""
        if inputStr.first == "." {
            inputStr = "0\(inputStr)"
        }
        
        // Find out the last character. If it is a ".", it should be replaced with "xx.00"
        if inputStr.last == "." {
            inputStr = "\(inputStr)00"
        }
        self.text = inputStr
    }
    
    func keyboard(_ type : Int) {
        switch type {
        case 1:
            self.keyboardType = .numberPad
        case 2:
            self.keyboardType = .emailAddress
        default:
            self.keyboardType = .default
        }
    }
    
}

extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }

    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient : CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
    
    
    /// To Apply theme gradient
//    @discardableResult
//    func applyThemeGradient() -> CAGradientLayer {
//        let gradientLayer =  CAGradientLayer()
//        gradientLayer.frame = self.bounds
//        gradientLayer.cornerRadius = self.layer.cornerRadius
//        gradientLayer.startPoint = CGPoint.init(x: 0, y: 1)
//        gradientLayer.endPoint   = CGPoint.init(x: 1, y: 0)
//        gradientLayer.colors = [UIColor.qBottomGradientColor.cgColor, UIColor.qTopGradientColor.cgColor]
//        self.layer.insertSublayer(gradientLayer, at: 0)
//        return gradientLayer
//    }
}

extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}

extension String {
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[1-9][0-9]{5}([0-9]{4})"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    
    var isNonEmpty: Bool {
        let trimmed = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return !trimmed.isEmpty
    }
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    var phoneNumberWithoutContryCode : String {
        
        if self.count <= 10 {
            return self
        }else {
            let countryCodeLength = self.count - 10
            return String(self.dropFirst(countryCodeLength))
        }
    }
    
    
    func toNumber() -> NSNumber? {
        if let intVal = Int(self) {
            return NSNumber(value:intVal)
        }
        return nil
    }
    
    func toFloat() -> Float? {
        if let val = Float(self) {
            return val
        }
        return nil
    }
    
    func removeLeadingTrailingSpaces() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
}



// ******************
// MARK:- Mask Color
// ******************
extension UIButton {
    
    @IBInspectable var maskImageColor : UIColor {
         get {
           return UIColor()
         } set {
            self.setImage(self.image(for: .normal)?.maskWithColor(color: newValue), for: .normal)
            self.setImage(self.image(for: .selected)?.maskWithColor(color: newValue), for: .selected)
            self.setImage(self.image(for: .highlighted)?.maskWithColor(color: newValue), for: .highlighted)
         }
    }
    
    @IBInspectable var maskBackgroundImageColor : UIColor {
         get {
           return UIColor()
         } set {
            self.setBackgroundImage(self.backgroundImage(for: .normal)?.maskWithColor(color: newValue), for: .normal)
            self.setBackgroundImage(self.backgroundImage(for: .selected)?.maskWithColor(color: newValue), for: .selected)
            self.setBackgroundImage(self.backgroundImage(for: .highlighted)?.maskWithColor(color: newValue), for: .highlighted)
         }
    }
    
}

extension UIImage {
    
    /// To change UIImage Color
    ///
    /// - Parameter color: image new color
    /// - Returns: UIImage with new color
    @objc public func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
    
    /// To Find the image size
    public func getSizeInMB() -> Double {
        
        let imgData = NSData(data: self.jpegData(compressionQuality: 1)!)
        let imageSize : Int = imgData.count
        //print("File Size : ", Double(imageSize) / 1000000.0)
        return Double(imageSize) / 1000000.0
        
    }
    
    /// To convert to Data
    public func getData(_ compression : CGFloat = 1) -> Data {
        
        let imgData = NSData(data: self.jpegData(compressionQuality: compression)!)
        return imgData as Data
        
    }
    
    
    /// To check whether the image contains a face
    public func isFaceImage() -> Bool {
        
        let ciImage = CIImage(cgImage: self.cgImage!)

        let options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options)!

        let faces = faceDetector.features(in: ciImage)

        if let face = faces.first as? CIFaceFeature {
            
            print("Found face at \(face.bounds)")
            return true

//            if face.hasLeftEyePosition {
//                print("Found left eye at \(face.leftEyePosition)")
//            }
//
//            if face.hasRightEyePosition {
//                print("Found right eye at \(face.rightEyePosition)")
//            }
//
//            if face.hasMouthPosition {
//                print("Found mouth at \(face.mouthPosition)")
//            }
        }else {
            
            return false
        }
        
    }
    
}



extension Data {
    private static let mimeTypeSignatures: [UInt8 : String] = [
        0xFF : "image/jpeg",
        0x89 : "image/png",
        0x47 : "image/gif",
        0x49 : "image/tiff",
        0x4D : "image/tiff",
        0x25 : "application/pdf",
        0xD0 : "application/vnd",
        0x46 : "text/plain",
        ]

    var mimeType: String {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return Data.mimeTypeSignatures[c] ?? "application/octet-stream"
    }
    
    
    /// Method will return document type
    func getMimeType() -> MimeType {
        
        var docType : MimeType = .Other
        switch self.mimeType {
        case "image/jpeg":
            docType = .Jpg
        case "image/png":
            docType = .Png
        case "application/pdf":
            docType = .PDF
        default:
            docType = .Other
        }
        
        return docType
    }
    
    
    /// Method to get UIImage from Data
    func getImage(_ scale : CGFloat = 1) -> UIImage {
        return UIImage(data: self, scale: scale)!
        
    }
}

enum MimeType : Int,Codable {
    case Png = 0
    case Jpg
    case PDF
    case Doc
    case Other
}

// ******************
// MARK:- Float
// ******************
extension Float {
    
    
    /// Convert Float value to string
    func toStringValue() -> String {
        
        if self == floorf(self) {
            return String(format: "%.f", self)
        }else {
            return String(format: "%.1f", self)
        }
    }
    
    /// Convert Float value to string. Will return double precetion string if its not an integer value
    func toAmountStringValue() -> String {
        
        if self == floorf(self) {
            return String(format: "%.f", self)
        }else {
            return String(format: "%.2f", self)
        }
    }
    
    func convertToString() -> String {
        
        return String(format: "%f", self)
    }
    
    /// Convert Float value to percentage string
    func toPercentageString() -> String {
        
        if self == floorf(self) {
            return "\(String(format: "%.f", self))%"
        }else {
            return "\(String(format: "%.2f%", self))%"
        }
    }
    
    /// To Convert value into currency string
//    func toCurrencyFormat() -> String {
//
//        let currencyNumber = NSNumber(value:self)
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .currency
//        formatter.locale = getLocale()
//        formatter.maximumFractionDigits = 2
//        formatter.minimumFractionDigits = 2
//        return formatter.string(from: currencyNumber)!
//
//    }
    
}




// ******************
// MARK:- Double
// ******************

extension Double {
    
    
    /// To Convert value into currency string
    func toCurrencyFormat() -> String {
    
        let currencyNumber = NSNumber(value:self)
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter.string(from: currencyNumber)!
    
    }
    
    
    /// Convert Float value to percentage string
    func toPercentageString() -> String {
        
        if self == floor(self) {
            return String(format: "%.f%", self)
        }else {
            return String(format: "%.2f%", self)
        }
    }
    
    /// Convert Float value to distance text
    func toDistanceString() -> String {
        
        if self == floor(self) {
            return String(format: "%.f km", self)
        }else {
            return String(format: "%.1f km", self)
        }
    }
}

extension Int {
    /// Convert Int value to string
    func toStringValue() -> String {
        return String(format: "%d", self)
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func checkStringLengthIsSuffice(_ length : Int) -> Bool {
        return self.count >= length
    }

}



extension Date {
    static var currentTimeStamp: Int64{
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    func toMilliSeconds() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }

    init(_ milliSeconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliSeconds / 1000))
        self.addTimeInterval(TimeInterval(Double(milliSeconds % 1000) / 1000 ))
    }
}

extension UIImage {
    // MARK: - UIImage+Resize
    func compressTo(_ expectedSizeInMb: Int) -> UIImage? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data : Data = self.jpegData(compressionQuality: compressingValue) {
            if data.count < sizeInBytes {
                needCompress = false
                imgData = data
            } else {
                compressingValue -= 0.1
            }
        }
    }

    if let data = imgData {
        if (data.count < sizeInBytes) {
            return UIImage(data: data)
        }
    }
        return nil
    }
    
    func compressImage() -> Data? {
    // Reducing file size to a 10th

        var actualHeight : CGFloat = self.size.height
        var actualWidth : CGFloat = self.size.width
        let maxHeight : CGFloat = 1136.0
        let maxWidth : CGFloat = 640.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        var compressionQuality : CGFloat = 0.5

        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else{
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }

        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = img.jpegData(compressionQuality: compressionQuality)else{
            return nil
        }
        return imageData
    }

    
}


extension UITableView{

    func indicatorView() -> UIActivityIndicatorView{
        var activityIndicatorView = UIActivityIndicatorView()
        let indicatorFrame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 40)
        activityIndicatorView = UIActivityIndicatorView(frame: indicatorFrame)
        activityIndicatorView.isHidden = false
        activityIndicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
        activityIndicatorView.color = .blue
        self.tableFooterView = activityIndicatorView
        return activityIndicatorView
    }

    func addLoading(_ indexPath:IndexPath, closure: @escaping (() -> Void)){
        indicatorView().startAnimating()
        if let lastVisibleIndexPath = self.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath && indexPath.row == self.numberOfRows(inSection: 0) - 1 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    closure()
                }
            }
        }
        indicatorView().isHidden = false
    }
    
    func startLoading() {
        indicatorView().isHidden = false
        indicatorView().startAnimating()
    }

    func stopLoading(){
        indicatorView().stopAnimating()
        indicatorView().isHidden = true
    }
}

extension NSNumber {
    
    func toInt() -> Int {
        return self.intValue
    }
    func toFloat() -> Float {
        return self.floatValue
    }
    func toDouble() -> Double {
        return self.doubleValue
    }
    func toString() -> String {
        return self.stringValue
    }
    
    func toDistanceString() -> String {
        let floatVal = self.floatValue
        return String(format:"%.1f",floatVal)
    }
}

extension Array where Element:Equatable{
    func removeDuplicates()->[Element]{
        var result = [Element]()
        for value in self{
            if !result.contains(value){
                result.append(value)
            }
        }
        return result
    }
}

extension UILabel {

    func startBlink() {
        UIView.animate(withDuration: 0.6,
              delay:0.0,
              options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
              animations: { self.alpha = 0 },
              completion: nil)
    }

    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}



extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}

extension UISearchBar {
    var textField: UITextField? {
        if #available(iOS 13.0, *) {
            return self.searchTextField
        } else {
            // Fallback on earlier versions
            for view in (self.subviews[0]).subviews {
                if let textField = view as? UITextField {
                    return textField
                }
            }
        }
        return nil
    }
}


extension String {
    
    func convertToDate(_ format: String = "dd-MM-yyyy") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: self) 
    }
    
}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

extension String {
    // formatting text for currency textField
    func currencyFormatting() -> String {
        if let value = Double(self) {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            formatter.currencySymbol = "Ksh"
            if let str = formatter.string(for: value) {
                return str
            }
        }
        return ""
    }
}

class RoundedButtonWithShadow: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height/2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 2.0
    }
}
