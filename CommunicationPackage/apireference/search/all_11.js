var searchData=
[
  ['query',['query',['../interface_c_s_messaging_address_validation.html#a0cad4dd5ae08b926f92b7c2ab2e5d04b',1,'CSMessagingAddressValidation::query()'],['../interface_c_s_messaging_query.html#a833d156635ae54ed83d46a73cbaa91e9',1,'CSMessagingQuery::query()']]],
  ['queryafter',['queryAfter',['../interface_c_s_messaging_query.html#a49eadd2f98ce0457945515ff069acae1',1,'CSMessagingQuery']]],
  ['querystring',['queryString',['../interface_c_s_contact_search_query_watcher_pair.html#add2f0753c9f193c32a7e3c7aec6cf223',1,'CSContactSearchQueryWatcherPair']]],
  ['queuedcallsnumber',['queuedCallsNumber',['../interface_c_s_queue_statistics.html#a97bcbca3d5676fdd4046fb6206952fa8',1,'CSQueueStatistics']]],
  ['queuedcallsthresholdreached',['queuedCallsThresholdReached',['../interface_c_s_queue_statistics.html#af8cb90a56d402baf9aa15967c229e255',1,'CSQueueStatistics']]],
  ['queuesize',['queueSize',['../interface_c_s_collaboration_receive_statistics.html#a95ddc3fd152f48d1c35662b538e061b2',1,'CSCollaborationReceiveStatistics']]],
  ['queuestatistics_3adidchangeactivestatus_3a',['queueStatistics:didChangeActiveStatus:',['../protocol_c_s_queue_statistics_delegate-p.html#a5b603b1a68a923d3a7314de544559c60',1,'CSQueueStatisticsDelegate-p']]],
  ['queuestatistics_3adidchangeskillname_3a',['queueStatistics:didChangeSkillName:',['../protocol_c_s_queue_statistics_delegate-p.html#a9d046dca1108598699622e7d420421ba',1,'CSQueueStatisticsDelegate-p']]],
  ['queuestatisticsdidupdateinformation_3a',['queueStatisticsDidUpdateInformation:',['../protocol_c_s_queue_statistics_delegate-p.html#a70ba61bc2cc91204d3ecf98403397137',1,'CSQueueStatisticsDelegate-p']]],
  ['queuestatisticslist',['queueStatisticsList',['../interface_c_s_agent_service.html#abf82b967d49d0eb1879a0340ab67a037',1,'CSAgentService']]],
  ['queuestatisticsmonitoringcapabilityforskillid_3a',['queueStatisticsMonitoringCapabilityForSkillId:',['../interface_c_s_agent_service.html#ac6be0d00c1bc17ed18ae9930a5eea7ef',1,'CSAgentService']]],
  ['queuetimethresholdreached',['queueTimeThresholdReached',['../interface_c_s_queue_statistics.html#a4ee39f7fb84ef59c10bb80ebea192566',1,'CSQueueStatistics']]]
];
