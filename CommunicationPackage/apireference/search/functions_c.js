var searchData=
[
  ['newproviderinstancewithaudio_3aandvideo_3a',['newProviderInstanceWithAudio:AndVideo:',['../interface_c_s_media_services_provider.html#a08f1ea1e48e72a62f1fc5b71b7383eb8',1,'CSMediaServicesProvider']]],
  ['newproviderinstancewithaudio_3aandvideo_3aanddispatchqueue_3a',['newProviderInstanceWithAudio:AndVideo:AndDispatchQueue:',['../interface_c_s_media_services_provider.html#a9c08992a04152bc59f76b22326fe0237',1,'CSMediaServicesProvider']]],
  ['numberofconversationswithunreadcontentforprovidertype_3a',['numberOfConversationsWithUnreadContentForProviderType:',['../interface_c_s_messaging_service.html#a22c3384f9fddf07927b63128437055d8',1,'CSMessagingService']]],
  ['numberofconversationswithunreadcontentsincelastaccessforprovidertype_3a',['numberOfConversationsWithUnreadContentSinceLastAccessForProviderType:',['../interface_c_s_messaging_service.html#ac8b7de53b9456dbb6366bf37e15dc37a',1,'CSMessagingService']]]
];
